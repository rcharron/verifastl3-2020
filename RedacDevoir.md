
# Manuel de rédaction d'un rendu de TD, d'un devoir maison ou d'un devoir sur table. 

## Comment maximiser sa note ?

### Avant le devoir, la préparation 

Tout d'abord relire son cours, et faire une fiche de cours si ce n'est pas déjà fait. Bien apprendre les définitions, refaire les exemples vu en cours. Laissez décanter le cours pendant la nuit avant de faire les exercices (il faut donc ne pas attendre la veille).

### Devant le sujet

Il faut commencer par lire le sujet en entier pour se faire une idée de ce qu'il y a dedant.
Comme cela, et surtout si vous êtes en temps limité, vous pouvez commencer par faire ce que vous maîtrisez le mieux.

### Devant un exercice

Rappelez-vous : il vous faudra justifier une réponse même si l'énoncé ne le précise pas (la seule exception est quand l'énoncé ne demande pas de justifier). 
Commencez par vous rappeler les définitions du cours. 
Si vous avez un algorithme dans l'énoncé prenez quelques minutes pour comprendre son fonctionnement. Prenez une entrée simple par exemple pour simuler son exécution.
Ensuite, il faudra sans doute une technique spécifique pour résoudre l'exercice. Faut-il raisonner par l'absurde, trouver un contre-exemple, un calcul direct peut-il suffire, faut-il distinguer deux cas ? 
Une fois que vous avez l'idée claire de la réponse vous pouvez passez à la rédaction.

La rédaction : décrivez ce que vous faites. Par exemple : pour une démonstration directe «Pour prouver la terminaison j'utilise un variant. Je me place dans l'ensemble … et j'utilise la relation d'ordre …, je pose mon variant P(m)=…», pour répondre faux avec un contre-exemple «Sur cet exemple … j'ai cette propriété …, cela est en conflit avec l'énoncé … donc on n'a pas … » Cela montre d'une part que vous connaissez le cours, et d'autre part cela explique pourquoi ce que vous faites justifie vous réponse.

Et si je sèche ? N'écrivez pas quelque chose si vous savez que c'est faux. Déjà ça se verra, ensuite ça ressemblera à de l'arnaque, et donc le correcteur ne sera pas content. Vous pouvez par contre, si vous avez un début d'idée, rédiger une ébauche de réponse, et justifier ce que vous écrivez. Par exemple : «Cette relation ne semble pas bien fondée, car il me semble … donc toute suite strictement décroissante devrait être finie», là on voit que vous connaissez le cours.

Si vous ne maîtrisez pas la syntaxe mathématique, faites des phrases en français, sinon le correcteur ne vous comprendra pas.
